import FieldFormik from '@/components/FieldFormik.vue';
import FormFormik from '@/components/FormFormik.vue';
import { createApp } from 'vue'
import App from './App.vue'

const app = createApp(App);

app.component('FieldFormik', FieldFormik);
app.component('FormFormik', FormFormik);

app.mount('#app');
